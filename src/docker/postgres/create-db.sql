-- Create Django Dev
CREATE DATABASE dev_django;
CREATE USER dev_optop WITH ENCRYPTED PASSWORD '<passwd>';
GRANT ALL PRIVILEGES ON DATABASE dev_django TO dev_optop;
\c dev_django
GRANT ALL PRIVILEGES ON SCHEMA public TO dev_optop;

-- Create Django Dev Manual
CREATE DATABASE devm_django;
CREATE USER devm_optop WITH ENCRYPTED PASSWORD '<passwd>';
GRANT ALL PRIVILEGES ON DATABASE devm_django TO devm_optop;
\c devm_django
GRANT ALL PRIVILEGES ON SCHEMA public TO devm_optop;

-- Create Django Prod
CREATE DATABASE prod_django;
CREATE USER prod_optop WITH ENCRYPTED PASSWORD '<passwd>';
GRANT ALL PRIVILEGES ON DATABASE prod_django TO prod_optop;
\c prod_django
GRANT ALL PRIVILEGES ON SCHEMA public TO prod_optop;

-- Create Gitlab
CREATE DATABASE gitlab;
CREATE USER gitlab_optop WITH ENCRYPTED PASSWORD '<passwd>';
GRANT ALL PRIVILEGES ON DATABASE gitlab TO gitlab_optop;
\c gitlab
GRANT ALL PRIVILEGES ON SCHEMA public TO gitlab_optop;

FROM jenkins/jenkins:lts

USER root

# apt 安裝必要套件
RUN apt-get update && apt-get install -y make wget gcc python3-openssl python3-venv python3-wheel libxslt-dev libzip-dev libldap2-dev libsasl2-dev python3-dev build-essential libssl-dev libffi-dev jq curl

USER jenkins

##################################################################################
# 安裝 Python
##################################################################################

# 安裝 pyenv
RUN curl -L https://raw.github.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash

# jenkins_home 不能用
RUN mv /var/jenkins_home/.pyenv /tmp/.pyenv

# 設定虛擬環境環境變數
ENV PATH="/tmp/.pyenv/versions/opt/bin:$PATH"
ENV PYENV_ROOT="/tmp/.pyenv"
ENV PATH="/tmp/.pyenv/bin:$PATH"

# 設定 pyenv
RUN eval "$(pyenv init --path)" && \
    eval "$(pyenv virtualenv-init -)"

# 安裝 python 3.12.4
RUN pyenv install 3.12.4

# 建立 virtualenv
RUN pyenv virtualenv 3.12.4 opt

##################################################################################
# 安裝 Nvm, Pnpm
##################################################################################
# Get the latest NVM version and install NVM
RUN mkdir -p /tmp/nvm

ENV NVM_DIR /tmp/nvm
ENV NODE_VERSION 20.13.1
RUN NVM_VERSION=$(curl --silent https://api.github.com/repos/nvm-sh/nvm/tags | jq -r '.[0].name') && \
    curl -o- "https://raw.githubusercontent.com/nvm-sh/nvm/${NVM_VERSION}/install.sh" | bash \
    && . $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default

# 讓使用者可以使用 npm 指令
ENV NODE_PATH $NVM_DIR/versions/node/v$NODE_VERSION/lib/node_modules
ENV PATH      $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

# Install pnpm
RUN npm install -g pnpm

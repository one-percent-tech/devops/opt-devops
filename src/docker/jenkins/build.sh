#! /bin/bash

docker build -t opt-jenkins .
docker tag opt-jenkins:latest 10.1.0.5:5000/opt-jenkins:latest
docker push 10.1.0.5:5000/opt-jenkins:latest

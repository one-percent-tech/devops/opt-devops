# DevOps 技術文件整理列表

- [DevOps 技術文件整理列表](#devops-技術文件整理列表)
  - [關於本專案的 DevOps 工作](#關於本專案的-devops-工作)
  - [相關部署文件](#相關部署文件)

## 關於本專案的 DevOps 工作

- [x] `K3S`：Kubernetes Cluster 高可用部署
- [x] `Rancher`：Kubernetes Cluster 管理
- [x] `Traefik`：Ingress Controller 自動簽署 Let's Encrypt 憑證，流量分發
- [x] `NFS-Server`：共享文件系統
- [x] `Jenkins`：CI/CD 自動化部署
- [x] `Easyrsa`：自建 CA 憑證管理
- [x] `Registry`：Docker 私有倉庫
- [x] `Databases`：建立多個資料庫
- [x] `GitLab`：Git 代碼管理
- [ ] `Prometheus`：監控
- [ ] `Grafana`：監控 Dashboard

## 相關部署文件

- [K3S](./docs/k3s.md)
- [Rancher](./docs/rancher.md)
- [Traefik](./docs/traefik.md)
- [Jenkins](./docs/jenkins.md)
- [NFS-Server](./docs/nfs-server.md)
- [Easyrsa](./docs/easyrsa.md)
- [Registry](./docs/registry.md)
- [Databases](./docs/databases.md)
- [GitLab](./docs/gitlab.md)
- [Prometheus](./docs/prometheus.md)
- [Grafana](./docs/grafana.md)

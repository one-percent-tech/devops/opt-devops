# Databases 安裝與設定

- [Databases 安裝與設定](#databases-安裝與設定)
  - [簡介](#簡介)
    - [PostgreSQL 安裝](#postgresql-安裝)
    - [Redis 安裝](#redis-安裝)
    - [mongodb 安裝](#mongodb-安裝)

## 簡介

關聯式資料庫：PostgreSQL

NoSQL 資料庫：MongoDB

Cache 資料庫：Redis

### PostgreSQL 安裝

參考 [databases.yml(postgres)](../src/docker/databases.yml)

1. 設定 create-db.sql 可以在初始化時建立對應的資料庫與使用者

   注：只有在初始化才會生效，關閉在啟動是不會再建立一次的

2. 安裝時需把 postgres 資料加一併建立，否則會出現錯誤

### Redis 安裝

參考 [databases.yml(redis)](../src/docker/databases.yml)

### mongodb 安裝

參考 [databases.yml(mongodb)](../src/docker/databases.yml)

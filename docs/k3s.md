# K3S 安裝手冊

- [K3S 安裝手冊](#k3s-安裝手冊)
  - [簡介](#簡介)
  - [安裝](#安裝)
    - [Master Node](#master-node)
    - [Worker Node](#worker-node)
    - [確認服務](#確認服務)
    - [Kubectl 使用指令](#kubectl-使用指令)
  - [移除](#移除)

## 簡介

K3s 是 Rancher Labs 開發的輕量級 Kubernetes 發行版，它的目標是在資源有限的環境中運行 Kubernetes。

## 安裝

[K3S 官方安裝手冊](https://rancher.com/docs/k3s/latest/en/installation/installation/)

`1 台 Master Node + 2 台 Worker Node` 的環境為例，透過以下步驟安裝 K3S：

### Master Node

- 使用 curl 安裝並取消預設安裝的 Traefik，如果不是用 root 使用者執行，加上 K3S_KUBECONFIG_MODE="644" 可以讓 kubeconfig 檔案的權限設定為 644。

```bash
curl -sfL https://get.k3s.io  | \
INSTALL_K3S_EXEC="--disable=traefik" \
K3S_KUBECONFIG_MODE="644" \
sh -
```

- 安裝完成後，取得 `node-token` 用於 Worker node join 使用：

```bash
sudo cat /var/lib/rancher/k3s/server/node-token
```

### Worker Node

- 使用 curl 安裝並指定 Master Node 的 IP 以及 `node-token`：

```bash
curl -sfL https://get.k3s.io | \
K3S_URL=https://<master-node-ip>:6443 \
K3S_TOKEN=<node-token> \
sh -
```

### 確認服務

- 安裝完成後，可以在 Master Node 上查看 Nodes 的狀態：

```bash
kubectl get nodes

>>>
NAME          STATUS   ROLES                  AGE   VERSION
k3s-worker1   Ready    <none>                 28h   v1.29.5+k3s1
k3s-worker2   Ready    <none>                 28h   v1.29.5+k3s1
k3s-master    Ready    control-plane,master   28h   v1.29.5+k3s1
```

- 也可以檢查所有服務狀態

```bash
sudo kubectl get all -n kube-system
```

為了部署方便可以將 Nods 加上 Label

```bash
kubectl label nodes k3s-master node=master
kubectl label nodes k3s-worker1 node=worker
kubectl label nodes k3s-worker2 node=worker

# 在上一個 worker 的分類，比較好指定 loading 比較重的服務
kubectl label nodes k3s-worker1 worker=1
kubectl label nodes k3s-worker2 worker=2
```

這個專案所有服務都會部署在 `app` namespace，所以可以建立一個 `app` namespace

```bash
kubectl create namespace app
```

### Kubectl 使用指令

查看 nodes 狀態

`kubectl get nodes`

查看 pods 狀態

`kubectl get pods`

查看 services 狀態

`kubectl get svc`

查看 deployments 狀態

`kubectl get deployments`

查看 ingress 狀態

`kubectl get ingress`

查看 logs

`kubectl logs <pod-name>`

查看 pod 的狀態

`kubectl describe pod <pod-name>`

建立 namespace

`kubectl create namespace <namespace-name>`

建立 Secret key

`kubectl create secret generic <secret-name> --from-literal=<key>=<value> -n <namespace-name>`

刪除所有服務

`kubectl delete all --all -n <namespace-name>`

如果有服務一直 terminating 可以使用以下指令強制刪除

`kubectl delete pod <pod-name> --grace-period=0 --force -n <namespace-name>`

Namespace 一直刪除不掉，可以使用以下指令強制刪除

```bash
kubectl get namespace "app" -o json \
  | tr -d "\n" | sed "s/\"finalizers\": \[[^]]\+\]/\"finalizers\": []/" \
  | kubectl replace --raw /api/v1/namespaces/app/finalize -f -
```

## 移除

- 移除 K3S Master Node

```bash
/usr/local/bin/k3s-uninstall.sh
```

- 移除 K3S Worker Node

```bash
/usr/local/bin/k3s-agent-uninstall.sh
```

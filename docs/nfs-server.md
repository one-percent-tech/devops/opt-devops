# NFS Server 安裝手冊

- [NFS Server 安裝手冊](#nfs-server-安裝手冊)
  - [簡介](#簡介)
  - [安裝](#安裝)
    - [安裝 NFS Server](#安裝-nfs-server)
    - [設定共享目錄](#設定共享目錄)
    - [設定 NFS Server exports](#設定-nfs-server-exports)

## 簡介

NFS（Network File System）是一種分散式檔案系統，允許用戶透過網絡訪問遠程文件。NFS Server 是 NFS 的服務端，用於提供文件共享服務。

## 安裝

### 安裝 NFS Server

```bash
sudo apt-get update
sudo apt-get install nfs-kernel-server
```

### 設定共享目錄

```bash
sudo mkdir -p /home/optop/nfs/<share-folder>
sudo chown nobody:nogroup /home/optop/nfs/<share-folder>
```

### 設定 NFS Server exports

```bash
/home/optop/<share-folder> <allowed-ip-with-mask>(rw,sync,no_root_squash,no_subtree_check,all_squash)

ex.
/home/optop/rancher 10.1.0.0/24(rw,sync,no_root_squash,no_subtree_check,all_squash)
```

# Rancher 安裝手冊

- [Rancher 安裝手冊](#rancher-安裝手冊)
  - [簡介](#簡介)
  - [安裝](#安裝)
  - [查詢並設置使用者名稱及密碼](#查詢並設置使用者名稱及密碼)
  - [選擇匯入現有叢集](#選擇匯入現有叢集)
    - [註冊 Kubernetes 叢集到 Rancher](#註冊-kubernetes-叢集到-rancher)

## 簡介

Rancher 是一個開源的容器管理平台，可以讓您在任何基礎設施上運行 Kubernetes。

## 安裝

本專案透過 Docker 安裝 Rancher。

參考檔案 [rancher.yml](../src/docker/rancher.yml)

## 查詢並設置使用者名稱及密碼

![Password](../src/imgs/rancher/password.jpg)

## 選擇匯入現有叢集

登錄後，點擊“Import Existing”選項，如下所示

![Import Existing Cluster](../src/imgs/rancher/import.jpg)

在下一個畫面中，在‘Cluster Management’下選擇“Generic”

![Generic Cluster](../src/imgs/rancher/generic.jpg)

填寫現有 Kubernetes 叢集的必要詳細信息：

- Cluster Name（叢集名稱）
- Description（描述）
- Member Roles（成員角色）

![Cluster Name](../src/imgs/rancher/generic-step.jpg)

點擊創建。

我們將獲得以下註冊 Kubernetes 叢集到 Rancher 的指示。
在 Kubernetes Master Node 叢集中運行這些命令。
在大多數情況下，我們在 Kubernetes 中使用自簽名證書，因此我們必須 highlighted。

![Cert Highlight](../src/imgs/rancher/cert-highlight.jpg)

### 註冊 Kubernetes 叢集到 Rancher

通過命令行連接到您的 Kubernetes 叢集，並運行以下命令進行註冊

![Cert Highlight Terminal](../src/imgs/rancher/cert-highlight-terminal.jpg)

上述命令將創建必要的 ClusterRole、命名空間、秘密和部署等。

等待 2 到 3 分鐘以啟動所有 Kubernetes 對象，然後返回 Rancher UI。

![Wait Connection](../src/imgs/rancher/wait-conn.jpg)

上述畫面確認我們的 Kubernetes 叢集已成功註冊到 Rancher UI。

現在，當您轉到 Rancher 的首頁時，您會看到那裡有一個更多的叢集可用，

![Select Cluster](../src/imgs/rancher/select-cluster.jpg)

當您點擊新註冊的叢集時，您將獲得如下所示的所有詳細信息。

![Result](../src/imgs/rancher/result.jpg)

# Registry 安裝手冊

- [Registry 安裝手冊](#registry-安裝手冊)
  - [簡介](#簡介)
  - [安裝](#安裝)
  - [設定](#設定)
  - [使用與驗證](#使用與驗證)
    - [Tag 一個標籤並 push 至 Registry](#tag-一個標籤並-push-至-registry)
    - [確認 registry 有哪些 images 的方法](#確認-registry-有哪些-images-的方法)
    - [k8s 中使用方法](#k8s-中使用方法)

## 簡介

Docker 私有倉庫，用於存儲 Docker 鏡像。

## 安裝

本專案透過 Docker 安裝 Registry

參考檔案 [registry.yml](../src/docker/registry.yml)

## 設定

- 設置 tls certs

參考 easyrsa 生成的憑證

[easyrsa.md](../docs/easyrsa.md)

- 安裝 apache2-utils 並生成 htpasswd

```bash
sudo apt-get install apache2-utils
htpasswd -Bc ~/nfs/registry/auth/htpasswd optop
```

- 測試登入 Docker 私有倉庫

```bash
docker login <registry-ip>:5000

>>>
Authenticating with existing credentials...
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
```

## 使用與驗證

### Tag 一個標籤並 push 至 Registry

```bash
# docker tag [OPTIONS] IMAGE[:TAG] [REGISTRYHOST/][USERNAME/]NAME[:TAG]

docker tag 518a41981a6a <domain|ip-with-port>/myImage

ex.
docker tag 518a41981a6a reg.my.com/myImage
docker tag 518a41981a6a 10.1.0.5:5000/myImage

docker push reg.my.com/myImage
```

### 確認 registry 有哪些 images 的方法

```bash
curl -k https://10.1.0.5:5000/v2/_catalog
>>>
{"repositories":["whoami"]}

curl -k https://10.1.0.5:5000/v2/whoami/tags/list
>>>
{"name":"whoami","tags":["v3"]}
```

### k8s 中使用方法

如果已經執行過 `docker login <domain|ip-with-port>`

這行指令正常登入後會產生 `/home/optop/.docker/config.json`

```json
/* /home/optop/.docker/config.json */
{
  "auths": {
    "10.1.0.5:5000": {
      "auth": "xxxxxxxxxxxxxxg="
    }
  }
}
```

把這個設定檔寫入 k8s secrets 中

```bash
kubectl create secret generic reg-key \
    --from-file=.dockerconfigjson=/home/optop/.docker/config.json \
    --type=kubernetes.io/dockerconfigjson
```

如果沒有設定過也可以透過下面這個方法只接寫入 secrets

```bash
kubectl create secret docker-registry reg-key \
--docker-server=<your-registry-server> \
--docker-username=<your-name> \
--docker-password=<your-pword> \
--docker-email=<your-email>
```

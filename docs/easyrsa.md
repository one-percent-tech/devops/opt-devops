# Easyrsa 安裝手冊

- [Easyrsa 安裝手冊](#easyrsa-安裝手冊)
  - [簡介](#簡介)
  - [安裝](#安裝)
  - [主機信任 Ca 憑證](#主機信任-ca-憑證)
  - [Docker 容器信任 Ca 憑證](#docker-容器信任-ca-憑證)

## 簡介

EasyRSA 是一個簡單的 PKI 工具，用於建立自己的 CA 憑證管理。本文將介紹如何使用 EasyRSA 來建立自己的 CA 憑證管理。

## 安裝

```bash
sudo apt install -y easy-rsa
```

將 easyrsa 加入 alias

```bash
echo "alias easyrsa=\"/usr/share/easy-rsa/easyrsa\"" >> ~/.bashrc
source ~/.bashrc
```

如果想要調整更多憑證內容可以參考這個
[官方文件](https://easy-rsa.readthedocs.io/en/latest/advanced/)

```bash
cp /usr/share/easy-rsa/vars.example /usr/share/easy-rsa/vars

# 修正 vars 檔案內容
```

生成 pki 架构，這個指令會在目前目錄下生成一個 pki 的目錄

```bash
easyrsa init-pki
```

生成 ca.crt

```bash
easyrsa build-ca nopass
```

生成 server.crt, server.key

```bash
easyrsa build-server-full server nopass
```

生成 client1.crt, client1.key

```bash
easyrsa build-client-full client1 nopass
```

如果需要簽署 SAN 可以透過以下指令

```bash
easyrsa --subject-alt-name="DNS:example.com,DNS:www.example.com,IP:192.1.1.1" build-server-full server nopass
```

生成 dh.pem

```bash
easyrsa gen-dh
```

查看服务端的配置文件

```bash
cd pki
ls ca.crt issued/ dh.pem private/
```

revoke 憑證

```bash
easyrsa revoke client1
```

生成 CRL

```bash
easyrsa gen-crl
```

## 主機信任 Ca 憑證

```bash
sudo cp ca.crt /usr/local/share/ca-certificates
sudo update-ca-certificates
```

## Docker 容器信任 Ca 憑證

創建 Docker 憑證目錄（如果不存在）：

```bash
sudo mkdir -p /etc/docker/certs.d/<domain/ip-with-port>
sudo mkdir -p /etc/docker/certs.d/10.1.0.5:5000
```

複製自簽名憑證到該目錄：

```bash
sudo cp ca.crt /etc/docker/certs.d/10.1.0.5:5000/
sudo systemctl restart docker
```

# Traefik 安裝手冊

- [Traefik 安裝手冊](#traefik-安裝手冊)
  - [簡介](#簡介)
  - [安裝](#安裝)
    - [Role-based Access Control (RBAC)](#role-based-access-control-rbac)
    - [Services](#services)
    - [Deployments](#deployments)
  - [註冊 Whoami 服務進行測試](#註冊-whoami-服務進行測試)

## 簡介

Traefik 是一個用於處理反向代理的工具，可以自動簽署 Let's Encrypt 憑證，並且可以自動發現後端服務，進行流量分發。

## 安裝

[官方文件](https://doc.traefik.io/traefik/user-guides/crd-acme/)

Install Traefik Resource Definitions (CRDs):

```bash
kubectl apply -f https://raw.githubusercontent.com/traefik/traefik/v3.0/docs/content/reference/dynamic-configuration/kubernetes-crd-definition-v1.yml
```

### Role-based Access Control (RBAC)

參考文件

- [role.yml](../src/kubernetes/traefik/deployements.yml)

```bash
kubectl apply -f role.yml -n app
```

### Services

參考文件

- [services.yml](../src/kubernetes/traefik/services.yml)

```bash
kubectl apply -f services.yml -n app
```

### Deployments

參考文件

- [deployments.yml](../src/kubernetes/traefik/deployements.yml)

```bash
kubectl apply -f deployments.yml -n app
```

---

## 註冊 Whoami 服務進行測試

參考文件

- [services.yml](../src/kubernetes/whoami/services.yml)
- [deployments.yml](../src/kubernetes/whoami/deployements.yml)
- [ingress.yml](../src/kubernetes/whoami/ingress.yml)

```bash
kubectl apply -f services.yml -n app
kubectl apply -f deployments.yml -n app
kubectl apply -f ingress.yml -n app
```

確認 Whoami 服務是否正常運作，一直重新整理確認 IP 是否有變化

```bash
curl https://whoami.one-percent-tech.xyz

>>>
Hostname: whoami-87c64fbb8-6wqf7
IP: 127.0.0.1
IP: ::1
---> IP: 10.42.2.27 <---
IP: fe80::7c4f:daff:fe2d:de98
RemoteAddr: 10.42.0.44:38984
GET / HTTP/1.1
Host: whoami.one-percent-tech.xyz
User-Agent: curl/7.81.0
Accept: */*
Accept-Encoding: gzip, br
Cdn-Loop: cloudflare
Cf-Connecting-Ip: 123.195.148.38
Cf-Ipcountry: TW
Cf-Ray: 8985cad07ad88b4b-HKG
Cf-Visitor: {"scheme":"https"}
X-Forwarded-For: 10.42.0.1
X-Forwarded-Host: whoami.one-percent-tech.xyz
X-Forwarded-Port: 443
X-Forwarded-Proto: https
X-Forwarded-Server: traefik-7456956b44-nt277
X-Real-Ip: 10.42.0.1
```

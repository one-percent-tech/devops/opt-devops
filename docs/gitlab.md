# Gitlab 安裝手冊

- [Gitlab 安裝手冊](#gitlab-安裝手冊)
  - [簡介](#簡介)
  - [安裝](#安裝)
    - [準備 NFS Server](#準備-nfs-server)
    - [安裝 Gitlab](#安裝-gitlab)
  - [Troubleshooting](#troubleshooting)

## 簡介

Gitlab 是一個基於 Git 的版本控制系統，提供了一個網頁界面，使得我們可以更加方便的管理代碼。

Gitlab 由 Gitlab 公司開發，是一個商業產品，但是 Gitlab 也提供了一個開源版本，可以自己部署在自己的伺服器上。

Gitlab 的開源版本和商業版本的功能基本一致，只是在性能和支持上有所不同。

在這裡，我們將介紹如何安裝 Gitlab 的開源版本。

## 安裝

> :bulb: 本教學建立在 k3s 的基礎上，並且所有的資料都透過 nfs server 儲存

### 準備 NFS Server

參考: [Using NFS with GitLab](https://docs.gitlab.com/ee/administration/nfs.html)

NfS server 的安裝可以參考 [nfs-server](./nfs-server.md)

1. 手動在 NFS Server 中建立以下資料夾

```bash
mkdir -p /home/optop/gitlab/config
mkdir -p /home/optop/gitlab/git-data
mkdir -p /home/optop/gitlab/gitlab-ci/builds
mkdir -p /home/optop/gitlab/gitlab-rails/shared
mkdir -p /home/optop/gitlab/gitlab-rails/uploads
```

2. 編輯 `/etc/exports`

```bash
/home/optop/gitlab/config 10.1.0.0/24(rw,sync,no_root_squash,no_subtree_check)
/home/optop/gitlab/.ssh 10.1.0.0/24(rw,sync,no_root_squash,no_subtree_check)
/home/optop/gitlab/gitlab-rails/uploads 10.1.0.0/24(rw,sync,no_root_squash,no_subtree_check)
/home/optop/gitlab/gitlab-rails/shared 10.1.0.0/24(rw,sync,no_root_squash,no_subtree_check)
/home/optop/gitlab/gitlab-ci/builds 10.1.0.0/24(rw,sync,no_root_squash,no_subtree_check)
/home/optop/gitlab/git-data 10.1.0.0/24(rw,sync,no_root_squash,no_subtree_check)
```

3. 重啟 NFS Server

```bash
# restart nfs server
sudo systemctl restart nfs-server
```

4. 測試 NFS Server

```bash
sudo exportfs -v
```

### 安裝 Gitlab

1. 在 config 中建立 `gitlab.rb`

   可以參考 [官方 gitlab.rb.temlate](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/files/gitlab-config-template/gitlab.rb.template)

   也可以等服務啟動後讓他自己先建立一份

2. 重點修改設定項目

   ```bash
    # 重要! 對外 Domain / IP
    external_url 'https://gitlab.one-percent-tech.xyz/'

    # 透過 ssh 時要給的參數，這裡用 IP 是因為 traefik 無法透過 domain 轉送 TCP 請求
    # 如果自己在 Router 上建立 DNS Server 就可以給 domain
    gitlab_rails['gitlab_ssh_host'] = '10.1.0.5'

    # 預設的 ssh user 就是 git，除非你自己有額外建立使用者，否則不建議修改
    gitlab_rails['gitlab_ssh_user'] = 'git'

    # 時區
    gitlab_rails['time_zone'] = 'Asia/Taipei'

    # Email Server 發送認證信需要，也可以自己搭 mail server
    # 但是比較容易被阻擋，可以直接參考網路上的申請教學
    # 這裡使用 google 的服務
    gitlab_rails['smtp_enable'] = true
    gitlab_rails['smtp_address'] = "smtp.gmail.com"
    gitlab_rails['smtp_port'] = 465
    gitlab_rails['smtp_user_name'] = "<username>@gmail.com"
    gitlab_rails['smtp_password'] = "<accessToken>"
    # gitlab_rails['smtp_domain'] = "example.com"
    gitlab_rails['smtp_authentication'] = "login"
    # gitlab_rails['smtp_enable_starttls_auto'] = true
    gitlab_rails['smtp_tls'] = true
    gitlab_rails['smtp_pool'] = false


    # 我們是透過 traefik 去向 letsencrypt 申請 tls 憑證
    # 所以要設定 trusted_proxies，這變指向的事 k3s 內部的 IP 地址
    # 因為我們也不知道最後 gitlab 被 DHCP 分配到的 IP 位置
    # /24 代表 10.42.0.1 ~ 10.42.0.254 這個範圍都信任
    gitlab_rails['trusted_proxies'] = ["10.42.0.1/24"]


    # 這裡是設定 gitlab 透過 google oauth2 登入
    gitlab_rails['omniauth_enabled'] = true
    gitlab_rails['omniauth_allow_single_sign_on'] = ['google_oauth2']
    # gitlab_rails['omniauth_sync_email_from_provider'] = 'saml'
    gitlab_rails['omniauth_sync_profile_from_provider'] = ['google_oauth2']
    gitlab_rails['omniauth_sync_profile_attributes'] = ['email']
    # gitlab_rails['omniauth_auto_sign_in_with_provider'] = 'saml'
    # gitlab_rails['omniauth_auto_link_saml_user'] = false
    gitlab_rails['omniauth_auto_link_user'] = ['google_oauth2']
    gitlab_rails['omniauth_external_providers'] = ['google_oauth2']
    gitlab_rails['omniauth_allow_bypass_two_factor'] = ['google_oauth2']
    gitlab_rails['omniauth_providers'] = [
       {
         "name" => "google_oauth2",
         "app_id" => "<app_id>",
         "app_secret" => "<app_secret>",
         "args" => { "access_type" => "offline", "approval_prompt" => "" }
       }
    ]

    # 透過 ssh 連線的 port，實際上內部還是 22 port 只是顯示在網頁上時會是 2222
    gitlab_rails['gitlab_shell_ssh_port'] = 2222
    gitlab_rails['gitlab_shell_git_timeout'] = 800


    # External Database 設定
    gitlab_rails['db_adapter'] = "postgresql"
    gitlab_rails['db_encoding'] = "utf8"
    #gitlab_rails['db_collation'] = nil
    gitlab_rails['db_database'] = "gitlab"
    gitlab_rails['db_username'] = "gitlab_optop"
    gitlab_rails['db_password'] = "1qaz@WSX"
    gitlab_rails['db_host'] = "10.1.0.10"
    gitlab_rails['db_port'] = "5332"

    # 如果使用外部資料庫，這裡要設定為 false，否則會自動安裝 postgresql
    postgresql['enable'] = false


    # Redis 設定
    gitlab_rails['redis_host'] = "10.1.0.10"
    gitlab_rails['redis_port'] = 6379
    gitlab_rails['redis_ssl'] = false
    gitlab_rails['redis_password'] = nil
    gitlab_rails['redis_database'] = 0
    gitlab_rails['redis_enable_client'] = true

    # !重要 Nginx 一定要開啟他會將資源檔 (assets) 及一些預設的設定一併設定好轉發出來
    nginx['enable'] = true
    #nginx['client_max_body_size'] = '250m'
    nginx['redirect_http_to_https'] = false
    nginx['redirect_http_to_https_port'] = 80

    ##! enable/disable 2-way SSL client authentication
    nginx['ssl_verify_client'] = "off"
   ```

參考以下文件安裝 Gitlab

[deployments.yml](../src/kubernetes/gitlab/deployments.yml)
[ingress.yml](../src/kubernetes/gitlab/ingress.yml)
[role.yml](../src/kubernetes/gitlab/role.yml)
[services.yml](../src/kubernetes/gitlab/services.yml)

```bash
kubectl apply \
-f deployments.yml \
-f ingress.yml \
-f role.yml \
-f services.yml \
-n gitlab
```

## Troubleshooting

1. 如果遇到儲存設定時出現 `500` 錯誤時可能是因為 `/etc/gitlab` 下的憑證與原先存在資料庫的不相符，建議將 `/etc/gitlab` 下的憑證刪除後及資料庫清除後重新啟動 gitlab
2. 或者可以嘗試使用下面的指令重新設定，但是不保證能解決問題

   ```bash
   # gitlab 設定無法儲存發生 500 error 的解決辦法, 在資料庫中刪除所有設定

   DELETE FROM ci_group_variables;
   DELETE FROM ci_variables;
   UPDATE projects SET runners_token = null, runners_token_encrypted = null;
   UPDATE namespaces SET runners_token = null, runners_token_encrypted = null;
   UPDATE application_settings SET runners_registration_token_encrypted = null;
   UPDATE ci_runners SET token = null, token_encrypted = null;
   UPDATE ci_builds SET token = null, token_encrypted = null;
   TRUNCATE web_hooks CASCADE;

   DELETE FROM application_settings;
   ```

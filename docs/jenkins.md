# Jenkins 安裝手冊

- [Jenkins 安裝手冊](#jenkins-安裝手冊)
  - [簡介](#簡介)
  - [安裝](#安裝)
  - [基本設定](#基本設定)
  - [進階 Pipeline](#進階-pipeline)
    - [使用者/密碼方式 \[withCredentials.usernamePassword\]](#使用者密碼方式-withcredentialsusernamepassword)
    - [使用者/密碼方式 \[withCredentials.sshUserPrivateKey\]](#使用者密碼方式-withcredentialssshuserprivatekey)

## 簡介

Jenkins 是一個開源的持續整合工具，用於自動化各種任務，包括構建、測試和部署軟體。
有了 Jenkins，只要提交代碼，就可以自動構建、測試和部署，從而節省了開發人員的時間。

本專案的目標以 Kubernetes 部署 Jenkins，並且與 Gitlab 整合，當專案 push 至特定 repository 時，Jenkins 會自動觸發建置流程。

## 安裝

前置準備

```bash
# 需要先 build image, 並推送到 docker registry
# 這裡的 image 在 K3S 會用到
cd src/docker/jenkins
./build.sh
```

參考文件

- [role.yml](../src/kubernetes/jenkins/role.yml)
- [services.yml](../src/kubernetes/jenkins/services.yml)
- [deployments.yml](../src/kubernetes/jenkins/deployements.yml)
- [ingress.yml](../src/kubernetes/jenkins/ingress.yml)

```bash
kubectl apply -f role.yml -n app
kubectl apply -f services.yml -n app
kubectl apply -f deployments.yml -n app
kubectl apply -f ingress.yml -n app
```

## 基本設定

1. 開啟 proxy 兼容模式

   管理 Jenkins > Security > 啟用代理伺服器相容模式

2. 安裝 Gitlab 插件

   管理 Jenkins > Plugins > Installed plugins > 查尋 GitLab Plugins 並安裝

3. 設定 GitLab api token

   **Jenkins**

   - 管理 Jenkins > Credentials > System > Global credentials > Add Credentials > System > Select Kind "GitLab Api token"

   - 管理 Jenkins > System > GitLab Tab

     Connection name: `gitlab-connection`

     GitLab host Url: `https://gitlab.com`

     Credentials: `Select or Create a credentials`

   **GitLab**

   - Gitlab > Person icon > Preferences > Access Tokens > Add new token

     give two permission `api`、`read_repository`

4. 透過 GitLab webhook 監聽 Repository push 事件並執行 Jenkins 腳本

   **Jenkins**

   - 新增作業 > Pipeline
   - 勾選 Build when a change is pushed to GitLab. GitLab webhook URL: `https://jenkins.one-percent-tech.xyz/project/<pipline_name>`
   - 打開 `進階` 選項並點擊 Generate 生成 `Secret Token` 這個 Token 會在 GitLab Webhooks 上使用
   - Pipeline Tab > Definition > 選擇 Pipeline script from SCM

     SCM: `Git`

     Repository URL: `git@gitlab.com:poliyka/<repo_name>` # 最後的 .git 刪掉不然會有問題

     Credentials: 新增一個 Certificate，這裡要使用 ssh-keygen 生成一個並貼上 private key 到這個欄位

     ```bash
     ssh-keygen -t ed25519 -C "integrating jenkins and gitlab"
     ```

   **Gitlab**

   - Person icon > Preferences > SSH KEY > 新增 KEY 並貼上剛剛生成的`公鑰`
   - Repository > Settings > Webhooks > Add new webhook

     URL: `https://jenkins.one-percent-tech.xyz/project/<pipline_name>`

     Secret token: 貼上剛剛在 Jenkins 生成的 `Secret Token`

     Trigger > Push events > Regular expression > `^(main)/`

5. 在 Repo 內建立 Jenkinsfile 先設定以下內容進行測試

   ```Jenkinsfile
   pipeline {
       agent any
       stages {
           stage('Example') {
               steps {
                   echo 'Hello World'
               }
           }
       }
   }
   ```

6. 對 Repo 進行 push 測試並檢視 Pipeline 中的建置歷程 > Console Output

   ```bash
   Started by GitLab push by poliyka
   Obtained Jenkinsfile from git git@gitlab.com:poliyka/<repo_name>
   > git rev-list --no-walk 07df62b04986fbf4665a59f888e42c64cc37ec05 # timeout=10
   ...
   [Pipeline] }
   [Pipeline] // stage
   [Pipeline] withEnv
   [Pipeline] {
   [Pipeline] stage
   [Pipeline] { (Example)
   [Pipeline] echo
   Hello World4
   [Pipeline] }
   [Pipeline] // stage
   [Pipeline] }
   [Pipeline] // withEnv
   [Pipeline] }
   [Pipeline] // node
   [Pipeline] End of Pipeline
   Finished: SUCCESS
   ```

## 進階 Pipeline

### 使用者/密碼方式 [withCredentials.usernamePassword]

```Groovy
def getHost() {
    def remote = [:]
    remote.name = 'Master'
    remote.host = '10.1.0.5'
    remote.allowAnyHosts = true
    return remote
}

pipeline {
   agent any
   stages {
      stage('Init Master') {
         steps {
            script {
                  master = getHost()
            }
         }
      }
      stage('Use Master') {
         steps {
            withCredentials([usernamePassword(credentialsId: 'k3s-pwd', passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
                  script {
                     master.user = USERNAME
                     master.password = PASSWORD
                     sshCommand remote: master, command: 'cat /etc/hostname'
                     sshCommand remote: master, command: "for i in {1..5}; do echo -n 'Loop \$i '; date ; sleep 1; done"
                  }
            }
         }
      }
   }
}
```

### 使用者/密碼方式 [withCredentials.sshUserPrivateKey]

- KeyFileVariable 'identity' 這個不要改
- 每一個 script 都必須重新設定 .user, .identityFile 因為機敏資訊的原因每一個 script 的 remote 資訊都會清空
- 如果出現 `con.jcraft.jsch.JSchException: Auth fail` 將公鑰放一份到 .ssh/`authorized_keys` 目錄下

```Groovy
def getHost() {
    def remote = [:]
    remote.name = 'Master'
    remote.host = '10.1.0.5'
    remote.allowAnyHosts = true
    return remote
}

pipeline {
   agent any
   stages {
      stage('Init Master') {
         steps {
            script {
                  master = getHost()
            }
         }
      }
      stage('Use Master') {
         steps {
            withCredentials([sshUserPrivateKey(credentialsId: 'k3s-ssh-key', keyFileVariable: 'identity', usernameVariable: 'USERNAME')]) {
                  script {
                     master.user = USERNAME
                     master.identityFile = identity
                     sshCommand remote: master, command: 'cat /etc/hostname'
                     sshCommand remote: master, command: "for i in {1..5}; do echo -n 'Loop \$i '; date ; sleep 1; done"
                  }
            }
         }
      }
   }
}
```
